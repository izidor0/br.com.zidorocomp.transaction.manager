this is a simple manual for compile and run future projects:
	for generate maven-wrapper:
		mvn -N io.takari:maven:wrapper
		mvn -N io.takari:maven:wrapper -Dmaven=3.5.2 (the version is optional, but the maven version needs to be the same as the server)
	for run with docker:
	./mvnw(mvnw.cmd for windows) package && java -jar target/br.com.zidorocomp.transaction.manager.jar
	
swagger doc:
	http://localhost:8080/swagger-ui.html#/	
