package br.com.zidorocomp.transaction.test;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.zidorocomp.transaction.manager.SpringApp;
import br.com.zidorocomp.transaction.manager.business.TransactionBusiness;
import br.com.zidorocomp.transaction.manager.business.Transactions;
import br.com.zidorocomp.transaction.manager.control.TransactionController;
import br.com.zidorocomp.transaction.manager.type.TransactionType;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
@ContextConfiguration(classes = { SpringApp.class })
public class AllTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TransactionController transactionController;

	@Test
	public void notHaveTransactionsTest() throws Exception {
		mockMvc.perform(get("transaction-manager/v1/statistics").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isNotFound());
	}

	@Test
	public void testThreadSafe() {
		
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				TransactionType transaction = new TransactionType();
				transaction.setAmount(50.5);
				transaction.setTimestamp(System.currentTimeMillis());
				TransactionBusiness transactionBusiness = new TransactionBusiness();
				transactionBusiness.saveTransaction(transaction);
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				TransactionType transaction = new TransactionType();
				transaction.setAmount(20.5);
				transaction.setTimestamp(System.currentTimeMillis());
				TransactionBusiness transactionBusiness = new TransactionBusiness();
				transactionBusiness.saveTransaction(transaction);
			}
		});
		
		t1.run();
		t2.run();
		
		try {
			Thread.sleep(500);
		} catch (Exception e) {
			//silent
		}
		
		assertEquals(2, Transactions.getInstance().getAll().size());
	}

	@Test
	public void saveTransactionTest() throws Exception {

		TransactionType transaction = new TransactionType();
		transaction.setAmount(50.5);
		transaction.setTimestamp(System.currentTimeMillis());

		TransactionBusiness transactionBusiness = new TransactionBusiness();
		assertEquals(transactionBusiness.saveTransaction(transaction), HttpStatus.CREATED.value());
	}
}
