package br.com.zidorocomp.transaction.manager.config.exception;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

public class TransactionException extends Exception implements Serializable {

	private static final long serialVersionUID = 1L;

	private HttpStatus httpStatus;

	public TransactionException(String message, Throwable cause, HttpStatus httpStatus) {
		super(message, cause);
		this.httpStatus = httpStatus;
	}
	
	public TransactionException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public TransactionException(Throwable cause, HttpStatus httpStatus) {
		super(cause);
		this.httpStatus = httpStatus;
	}
	
	public TransactionException(Throwable cause) {
		super(cause);
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	
}