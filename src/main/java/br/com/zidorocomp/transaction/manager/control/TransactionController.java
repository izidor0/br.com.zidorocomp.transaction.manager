package br.com.zidorocomp.transaction.manager.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.zidorocomp.transaction.manager.business.TransactionBusiness;
import br.com.zidorocomp.transaction.manager.config.exception.TransactionException;
import br.com.zidorocomp.transaction.manager.type.StatisticType;
import br.com.zidorocomp.transaction.manager.type.TransactionType;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "transaction-manager/v1/")
public class TransactionController {

	@Autowired
	TransactionBusiness transactionBusiness;

	@RequestMapping(value = "transactions", method = RequestMethod.POST, consumes = {MediaType.ALL_VALUE})
	@ApiOperation(value = "Save a transaction")
	public @ResponseBody ResponseEntity<Object> saveTransaction(@RequestBody TransactionType transaction) throws TransactionException {
		try {
			HttpStatus httpStatus = HttpStatus.valueOf(transactionBusiness.saveTransaction(transaction));
			return new ResponseEntity<>(httpStatus);
		} catch (Exception e) {
			throw new TransactionException(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "statistics", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Returns the statistic of all transactions")
	public @ResponseBody ResponseEntity<StatisticType> getStatistcs() throws TransactionException {
		try {
			StatisticType statistic = transactionBusiness.getStatistcs();
			if (statistic != null) {
				return new ResponseEntity<StatisticType>(statistic, HttpStatus.OK);
			}
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			throw new TransactionException(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
