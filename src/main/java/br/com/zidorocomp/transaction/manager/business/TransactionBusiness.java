package br.com.zidorocomp.transaction.manager.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.zidorocomp.transaction.manager.type.StatisticType;
import br.com.zidorocomp.transaction.manager.type.TransactionType;

@Component
public class TransactionBusiness {
	
	public int saveTransaction(TransactionType transaction) {
		
		if (transaction.getTimestamp() == 0)
			return 400;
		
		long diff = (System.currentTimeMillis() - transaction.getTimestamp()) / 1000 ;
		if (diff <= 60) {
			Transactions.getInstance().add(transaction);
			return 201;
		}
		return 204;
	}
	
	public StatisticType getStatistcs() {
		
		List<TransactionType> transactions = Transactions.getInstance().getAll();
		if (!transactions.isEmpty()) {
			
			double sum = 0.0, max = 0.0, avg = 0.0;
			Double min = null;
			int count = transactions.size();

			for (TransactionType transaction : transactions) {
				sum = sum + transaction.getAmount();

				if (max < transaction.getAmount()) {
					max = transaction.getAmount();
				}

				if (min == null) {
					min = transaction.getAmount();
					
				} else if (transaction.getAmount() < min) {
					min = transaction.getAmount();
				}
			}
			avg = sum / count;
			return new StatisticType(sum, min, max, avg, count);
		}
		return null;
	}
	
}
