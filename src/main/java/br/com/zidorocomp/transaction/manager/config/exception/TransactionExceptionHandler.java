package br.com.zidorocomp.transaction.manager.config.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class TransactionExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(TransactionException.class)
	public final ResponseEntity<TransactionException> handleAllExceptions(TransactionException transactionException) {
		return new ResponseEntity<TransactionException>(transactionException, transactionException.getHttpStatus());
	}

}