package br.com.zidorocomp.transaction.manager.business;

import java.util.ArrayList;
import java.util.List;

import br.com.zidorocomp.transaction.manager.type.TransactionType;

public final class Transactions {
	
	private static final Transactions SINGLETON = new Transactions();
	
	private List<TransactionType> list = new ArrayList<>();

	public static Transactions getInstance() {
		return SINGLETON;
	}
	
	public synchronized void add(TransactionType transaction) {
		list.add(transaction);
	}
	
	public synchronized void remove(int transactionIndex) {
		list.remove(transactionIndex);
	}
	
	public synchronized  List<TransactionType> getAll() {
		return list;
	}
}
