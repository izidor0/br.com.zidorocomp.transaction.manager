package br.com.zidorocomp.transaction.manager.type;

public class StatisticType {

	private double sum;
	private double min;
	private double max;
	private double avg;
	private int count;
	
	public StatisticType() {
	}

	public StatisticType(double sum, double min, double max, double avg, int count) {
		super();
		this.sum = sum;
		this.min = min;
		this.max = max;
		this.avg = avg;
		this.count = count;
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getAvg() {
		return avg;
	}

	public void setAvg(double avg) {
		this.avg = avg;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}